# Matrixish Theme

Matrixish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-matrixish_code.png](./images/sjsepan-matrixish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-matrixish_codium.png](./images/sjsepan-matrixish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-matrixish_codedev.png](./images/sjsepan-matrixish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-matrixish_ads.png](./images/sjsepan-matrixish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-matrixish_theia.png](./images/sjsepan-matrixish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-matrixish_positron.png](./images/sjsepan-matrixish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/28/2025
