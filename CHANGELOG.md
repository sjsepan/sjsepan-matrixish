# Matrixish Color Theme - Change Log

## [0.3.2]

- tweak widget/notification borders and backgrounds

## [0.3.1]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.3.0]

- brighten contrasting panels
- dim borders
- retain v0.2.3 for those that prefer earlier style

## [0.2.3]

- update readme and screenshot

## [0.2.2]

- fix source control graph badge colors: dim scmGraph.historyItemRefColor
- fix titlebar border, FG/BG in custom mode

## [0.2.1]

- fix manifest and pub WF

## [0.2.0]

- dimmer buttons BG so as not to compete visually with project names in Git sidebar
- fix manifest repo links
- retain v0.1.3 for those that prefer earlier style

## [0.1.3]

- make badge BG transparent

## [0.1.2]

- fix minimap section header BG

## [0.1.1]

- swap button.FG.BG

## [0.1.0]

- alter syntax color set for more distinction
- retain prior version for those who prefer that style

## [0.0.10]

- dim certain borders (editorgroup, header)
- brighten borders on sidebar sectionheader title

## [0.0.9]

- fix terminal cursor BG
- brighten input border
- darken input BG
- brighten various selection BG
- dim input placeholder FG

## [0.0.8]

- fix title in change log
- fix statusbar BG, border for nofolder, etc.
"statusBar.debuggingBackground": "#010B02",
"statusBar.debuggingBorder": "#097B22",
"statusBar.noFolderBackground": "#010B02",
"statusBar.noFolderBorder": "#097B22",

## [0.0.7]

- fix scrollbar, minimap slider transparencies
dark:
"minimapSlider.activeBackground": "#ffffff40",
"minimapSlider.background": "#ffffff20",
"minimapSlider.hoverBackground": "#ffffff60",
"scrollbarSlider.activeBackground": "#ffffff40",
"scrollbarSlider.background": "#ffffff20",
"scrollbarSlider.hoverBackground": "#ffffff60",
- fix completion list contrast:
editorSuggestWidget.selectedBackground OK
list.hoverBackground darkened

## [0.0.6]

- fix minimap selection highlight

## [0.0.5]

- fix badges
- fix pkg readme screenshot url

## [0.0.4]

- lightened BG on activity bar, breadcrumb, panel title area, status bar
- dim non-current line number FG
- reverse activity bar badge FG / BG
- highlight current status bar item on hover; reverse FG / BG
- dim borders

## [0.0.3]

- fix category from light to dark for real: "uiTheme": "vs-dark" in package.json

## [0.0.2]

- fix category from light to dark
- add contrasting values for keybindingLabel

## [0.0.1]

- Initial release
